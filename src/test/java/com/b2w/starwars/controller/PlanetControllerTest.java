package com.b2w.starwars.controller;

import com.b2w.starwars.StarwarsApplication;
import com.b2w.starwars.domain.Planet;
import com.b2w.starwars.repository.PlanetRepository;
import com.b2w.starwars.service.PlanetService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.b2w.starwars.controller.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StarwarsApplication.class)
public class PlanetControllerTest {

    private static final String PLANET_ENDPOINT = "/planets";

    private static final String NOME_ID_IMPROVAVEL = "XYZ123#456ZYX";
    private static final String DEFAULT_NAME = "Naboo";
    private static final String DEFAULT_CLIMATE = "Climate Naboo";
    private static final String DEFAULT_TERRAIN = "Terrain Naboo";

    private static final String EXPECTED_NAME = DEFAULT_NAME;
    private static final String EXPECTED_CLIMATE = DEFAULT_CLIMATE.toLowerCase();
    private static final String EXPECTED_TERRAIN = DEFAULT_TERRAIN.toLowerCase();


    private static final Integer DEFAULT_QT_FILMS = 4;

    private static String PAGE_SIZE;

    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private PlanetService planetService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restPlanetMockMvc;

    private Planet planet;


    @Autowired
    private Environment env;

    @Before
    public void setup() {

        PAGE_SIZE = env.getProperty("page.size").toString();


        MockitoAnnotations.initMocks(this);
        final PlanetController planetController = new PlanetController(planetService);


        this.restPlanetMockMvc = MockMvcBuilders.standaloneSetup(planetController)
                .setConversionService(createFormattingConversionService())
                .setMessageConverters(jacksonMessageConverter).build();
    }


    /**
     * Cria uma entidade para o teste.
     */
    public static Planet createEntity() {
        Planet planet = new Planet(DEFAULT_NAME, DEFAULT_CLIMATE, DEFAULT_TERRAIN);
        planet.setFilmQuantity(DEFAULT_QT_FILMS);
        return planet;
    }

    @Before
    public void initTest() {
        /**
         * Se desejar limpar a base por completo antes de iniciar os testes:
         *
         */
        planetRepository.deleteAll();
        planet = createEntity();
    }

    @Test
    public void createPlanet() throws Exception {
        int databaseSizeBeforeCreate = planetRepository.findAll().size();

        // Cria o Planeta
        restPlanetMockMvc.perform(post(PLANET_ENDPOINT)
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(planet)))
                .andExpect(status().isCreated());

        // Valida o Planeta na base de dados
        List<Planet> planetList = planetRepository.findAll();
        assertThat(planetList).hasSize(databaseSizeBeforeCreate + 1);
        Planet testPlanet = planetList.get(planetList.size() - 1);
        assertThat(testPlanet.getName()).isEqualTo(EXPECTED_NAME);
        assertThat(testPlanet.getClimate()).isEqualTo(EXPECTED_CLIMATE);
        assertThat(testPlanet.getTerrain()).isEqualTo(EXPECTED_TERRAIN);
        assertThat(testPlanet.getFilmQuantity()).isEqualTo(DEFAULT_QT_FILMS);
    }

    @Test
    public void getAllPlanets() throws Exception {
        // Inicializa a base.
        planetService.create(planet);

        // Solicita um get que listará todos os planetas da base.
        ResultActions resultActions =
                restPlanetMockMvc
                        .perform(get(PLANET_ENDPOINT + "?limit=" + PAGE_SIZE ));

        // Facilita a análise do resultado no caso de depuração dos testes.
        String content = resultActions.andReturn().getResponse().getContentAsString();

                resultActions
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(jsonPath("$.content.[*].id").value(hasItem(planet.getId())))
                        .andExpect(jsonPath("$.content.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                        .andExpect(jsonPath("$.content.[*].climate").value(hasItem(EXPECTED_CLIMATE.toString())))
                        .andExpect(jsonPath("$.content.[*].terrain").value(hasItem(EXPECTED_TERRAIN.toString())))
                        .andExpect(jsonPath("$.content.[*].filmQuantity").value(DEFAULT_QT_FILMS));

    }

    @Test
    public void getPlanetById() throws Exception {
        // Inicializa a base.
        planetService.create(planet);

        // Solicita o planeta através de um GET pelo id.
        ResultActions resultActions =
                restPlanetMockMvc
                        .perform(get(PLANET_ENDPOINT + "/{id}", planet.getId()));

        // Facilita a análise do resultado no caso de depuração dos testes.
        String content = resultActions.andReturn().getResponse().getContentAsString();

                resultActions
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(jsonPath("$.id").value(planet.getId()))
                        .andExpect(jsonPath("$.name").value(EXPECTED_NAME.toString()))
                        .andExpect(jsonPath("$.climate").value(EXPECTED_CLIMATE.toString()))
                        .andExpect(jsonPath("$.terrain").value(EXPECTED_TERRAIN.toString()))
                        .andExpect(jsonPath("$.filmQuantity").value(DEFAULT_QT_FILMS));

    }

    @Test
    public void getPlanetByName() throws Exception {
        // Inicializa a base.
        planetService.create(planet);

        // Solicita o planeta através de um GET pelo nome.
        ResultActions resultActions =
                restPlanetMockMvc
                        .perform(get(PLANET_ENDPOINT + "?name=" + DEFAULT_NAME + "&limit=" + PAGE_SIZE ));

                // Facilita a análise do resultado no caso de depuração dos testes.
                String content = resultActions.andReturn().getResponse().getContentAsString();

                resultActions
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(jsonPath("$.content.[*].id").value(hasItem(planet.getId())))
                        .andExpect(jsonPath("$.content.[*].name").value(hasItem(EXPECTED_NAME.toString())))
                        .andExpect(jsonPath("$.content.[*].climate").value(hasItem(EXPECTED_CLIMATE.toString())))
                        .andExpect(jsonPath("$.content.[*].terrain").value(hasItem(EXPECTED_TERRAIN.toString())))
                        .andExpect(jsonPath("$.content.[*].filmQuantity").value(DEFAULT_QT_FILMS));

    }

    @Test
    public void getNonExistingPlanetById() throws Exception {
        // Solicita um planeta através de um id improvável.
        restPlanetMockMvc.perform(get(PLANET_ENDPOINT + "/{id}", NOME_ID_IMPROVAVEL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getNonExistingPlanetByName() throws Exception {
        // Solicita um planeta através de um nome improvável.
        ResultActions resultActions =
        restPlanetMockMvc.perform(get(PLANET_ENDPOINT + "?name=" + NOME_ID_IMPROVAVEL + "&limit=" + PAGE_SIZE ));
        // Facilita a análise do resultado no caso de depuração dos testes.
        String content = resultActions.andReturn().getResponse().getContentAsString();

        resultActions
                .andExpect(status().isNotFound());
    }

    @Test
    public void deletePlanet() throws Exception {
        // Inicializa a base.
        planetService.create(planet);

        int databaseSizeBeforeDelete = planetRepository.findAll().size();

        // Recupera os dados do planeta pelo id.
        restPlanetMockMvc.perform(delete(PLANET_ENDPOINT + "/{id}", planet.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Verifica que a base de dados está vazia.
        List<Planet> planetList = planetRepository.findAll();
        assertThat(planetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
