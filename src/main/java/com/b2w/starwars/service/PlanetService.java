package com.b2w.starwars.service;

import com.b2w.starwars.domain.Planet;
import com.b2w.starwars.service.exceptions.ExistingPlanetNameException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.Optional;

/**
 * @author marcelo.chiarella@gmail.com
 * 31/03/2018
 **/

public interface PlanetService {

    Page<Planet> getAll(Pageable pageable);

    Optional<Planet> findById(String id);

    Page<Planet> findByName(String name, Pageable pageable);

    Planet create(Planet planet) throws ExistingPlanetNameException;

    void update(Planet planet);

    void delete(String id);

    boolean exists(Planet planet);

    void deleteById(String id);


}
