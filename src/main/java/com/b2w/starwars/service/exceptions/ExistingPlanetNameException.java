package com.b2w.starwars.service.exceptions;

public class ExistingPlanetNameException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public ExistingPlanetNameException(String msg){
        super(msg);
    }

    public ExistingPlanetNameException(String msg, Throwable cause){
        super(msg, cause);
    }
}
