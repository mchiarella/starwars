package com.b2w.starwars.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

/**
 * @author marcelo.chiarella@gmail.com
 * Criação dos headers HTTP
 **/

public class HeaderUtil {
        private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

        private HeaderUtil() {
        }

        public static HttpHeaders createAlert(String message, String param) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("X-smartLogApp-alert", message);
            headers.add("X-smartLogApp-params", param);
            return headers;
        }

        public static HttpHeaders createEntityCreationAlert(String entityName, String param) {
            return createAlert(entityName + " id:" + param + ", registro criado.", param);
        }

        public static HttpHeaders createEntityUpdateAlert(String entityName, String param) {
            return createAlert(entityName + " id:" + param + ", registro atualizado.", param);
        }

        public static HttpHeaders createEntityDeletionAlert(String entityName, String param) {
            return createAlert(entityName + " id:" + param + ", registro removido.", param);
        }

        public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("X-smartLogApp-error", defaultMessage);
            headers.add("X-smartLogApp-params", entityName);
            return headers;
        }
    }
