package com.b2w.starwars.repository;

import com.b2w.starwars.domain.Planet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * @author marcelo.chiarella@gmail.com
 * 30/03/2018
 **/

public interface PlanetRepository  extends MongoRepository<Planet, String>{
    List<Planet> findByName(@Param("name") String name);

    // Muito mais versátil, busca por qualquer parte do nome (case insensitive)
    @Query("{ 'name': { $regex: ?0, $options: 'i' } }")
    Page<Planet> findByNameLike(String txtLike, Pageable pageable);

}
