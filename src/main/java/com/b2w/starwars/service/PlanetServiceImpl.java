package com.b2w.starwars.service;

import com.b2w.starwars.domain.Planet;
import com.b2w.starwars.repository.PlanetRepository;
import com.b2w.starwars.service.exceptions.ExistingPlanetNameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * @author marcelo.chiarella@gmail.com
 *
 * @updated 07/04/2018
 **/

@Service
public class PlanetServiceImpl implements PlanetService {

    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private RemoteService remoteService;


    @Override
    public Page<Planet> getAll(Pageable pageable) {
        return planetRepository.findAll(pageable);
    }

    @Override
    public Optional<Planet> findById(String id) {
        return planetRepository.findById(id);
    }

    @Override
    public Page<Planet> findByName(String name, Pageable pageable) {
        return planetRepository.findByNameLike(name, pageable);
    }

    @Override
    public Planet create(Planet planet) throws ExistingPlanetNameException {
        String planetName = planet.getName();

        Integer filmQuantity = remoteService.getFilmQuantity(planetName.toLowerCase());
        planet.setFilmQuantity(filmQuantity);

        // Padronização de minúsculas para climate e terrain
        planet.setClimate(planet.getClimate().toLowerCase());
        planet.setTerrain(planet.getTerrain().toLowerCase());

        // Impede novo planeta de ter um id que não gerado automaticamente pelo mongo.
        if (planet.getId() != null) {
            planet.setId(null);
        } else if (planetName == null || planetName.trim().equals("")) {
            return null;
        }

        if (!planetRepository.findByName(planetName).isEmpty()){
            throw new ExistingPlanetNameException("Planeta de nome " + planetName + " já consta na base de dados.");
        }
        return planetRepository.save(planet);
    }

    @Override
    public void update(Planet planet) {
    }

    @Override
    public void delete(String id) {
    }

    @Override
    public void deleteById(String id){
        planetRepository.deleteById(id);
        return;
    }

    @Override
    public boolean exists(Planet planet) {
        return false;
    }
}