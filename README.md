# API REST para Star Wars



### Características

Para cada planeta, os seguintes dados são obtidos do banco de dados da aplicação:

- `name` (nome)
- `climate` (clima)
- `terrain` (terrain)
- `filmQuantity` (quantidade de aparições em filmes)
 
 > A informação sobre a quantidade é obtida por acesso remoto à API pública do [Star Wars](https://swapi.co/)



### Funcionalidades

- Adicionar um planeta (com nome, clima e terreno)
- Listar planetas
- Buscar por nome
- Buscar por id
- Remover planeta



### Linguagem e Framework

Java + Spring Boot



### Banco

MongoDB



### Testes Unitários

JUnit



### Endpoints e Métodos

> Para manter a consistência com a REST API pública, os nomes do recurso e seus atributos estão todos em inglês: `planet`, `climate`, `name`, `terrain` etc.



#### Adicionar um planeta: método `POST` em:

```
https://starwarsb2w.herokuapp.com/planets
```



com o payload exemplificado a seguir:

```
{
	"name": "Dagobah",
	"climate": "murky",
	"terrain": "swamp, jungles"
}
```

> Obs: a quantidade de participação em filmes é atribuída automaticamente caso o nome do planeta esteja entre os 61 da REST API pública. Esse Mapa é criado assim que a aplicação é inicializada e ocorre somente uma vez. Em caso de falha na inicialização, novas tentativas são efetuadas a cada novo POST.
> O serviço de criação de novo planeta verifica a existência de outro de mesmo nome na base, impedindo a criação e retornando uma mensagem de erro com Status 400 (BAD REQUEST). Os atributos `climate` e `terrain` tem seu conteúdo transformado para caixa baixa para manter consistência com a API pública.



#### Buscar todos planetas: método `GET` sem qualquer payload no body em:

```
https://starwarsb2w.herokuapp.com/planets
```



#### Buscar por nome: método `GET` (sem payload) em:

```
https://starwarsb2w.herokuapp.com/planets?name=ParteDoNomeDoPlaneta
```

> `Obs` Optei por poder especificar apenas um trecho do nome, retornando uma lista.



#### Buscar por id: método `GET` (sem payload) em:

```
https://starwarsb2w.herokuapp.com/planets/idDoPlaneta
```



#### Remover planeta: método `DELETE` (sem payload) em:

```
https://starwarsb2w.herokuapp.com/planets/idDoPlaneta
```


#### Paginação

Os métodos que retornam todos os planetas ou alguns (quando a busca ocorre por nome) retornam listas devidamente paginadas. Segue um exemplo de consulta com paginação:

https://starwarsb2w.herokuapp.com/planets?name=d&page=0&limit=5&direction=DESC

Nesse caso a primeira página é exibida (indice 0), com, no máximo, 5 registros por página e ordem alfabética decrescente (o campo name é o padrão para a ordenação)



#### Ordenação do Registros

Como mencionado, a ordenação default é a alfabética por name, no entanto, pode-se especificar outro atributo para a ordenação, como por exemplo "climate" atribuindo uma especificação para o parâmetro orderby, como no exemplo a seguir:

https://starwarsb2w.herokuapp.com/planets?page=0&limit=24&orderby=climate&direction=DESC


#### Documentação Swagger

Para facilitar uma consulta rápida e possibilidade de postar alguns registros, estou publicando um esboço da documentação da API REST utilizando o Swagger. Ainda há melhorias para fazer, como por exemplo, especificar mais detalhadamente os parâmetros de entrada (o de paginação está com uma constante string que deve ser substituída por um número para que funcione adequadamente) e também os status retornados por cada método (está muito genérico).

[https://starwarsb2w.herokuapp.com/swagger-ui.html](https://starwarsb2w.herokuapp.com/swagger-ui.html)



#### Testes Unitários

Foram implementados e ajudaram muito no desenvolvimento, principalmente no refactoring de código. Foram desenvolvidos apenas testes específicos para os endpoints da aplicaçao e estão listados a seguir:

- createPlanet
- getAllPlanets
- getPlanetById
- getPlanetByName
- getNonExistingPlanetById
- getNonExistingPlanetByName
- deletePlanet



#### Melhorias

Eu apontaria, como melhorias:

- Um desenvolvimento mais aprofundado dos testes unitários, criando uma base de mockup diferente da base que está em "produção".
- Criação de testes específicos de conexão, do mapa de contagem de filmes que é construído logo no start da aplicação.
- Criação de mais tratamentos de erros personalizados.