package com.b2w.starwars.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author marcelo.chiarella@gmail.com
 * 05/04/2018
 **/
@Service
public class RemoteService {

    HashMap<String, Integer> planetFilmQuantity;

    private final Logger LOG = LoggerFactory.getLogger(RemoteService.class);

    public void initializeService() {
        if (planetFilmQuantity!=null){
            return;
        }
        try {
            LOG.info("Coletando a quantidade de aparições em filmes...");
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

            planetFilmQuantity = new HashMap<>();

            ResponseEntity<Object> response = null;
            String apiUrlNext = "https://swapi.co/api/planets";
            while ((apiUrlNext != null) && (apiUrlNext != "")) {
                response = restTemplate.exchange(apiUrlNext, HttpMethod.GET, entity, Object.class);
                LinkedHashMap<String, Object> responseBody = (LinkedHashMap<String, Object>) response.getBody();
                apiUrlNext = (String) responseBody.get("next");
                ArrayList<Object> results = (ArrayList<Object>) responseBody.get("results");

                if (results.size() > 0) {
                    for (Object record : results) {
                        String name = (String) ((LinkedHashMap<String, Object>) record).get("name");
                        Integer quantity = null;
                        ArrayList<String> films = (ArrayList<String>) (((LinkedHashMap<String, Object>) record).get("films"));
                        if (films != null) {
                            quantity = films.size();
                        }
                        planetFilmQuantity.put(name.toLowerCase(), quantity);
                    }
                }
            }
            if (planetFilmQuantity.size()>0){
                LOG.info("Coleta finalizada");
            } else {
                LOG.info("Problema na coleta das informações de filmes.");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public Integer getFilmQuantity(String filmName){
        initializeService();
        if(planetFilmQuantity!=null){
            return planetFilmQuantity.get(filmName);
        }
        return null;
    }
}
