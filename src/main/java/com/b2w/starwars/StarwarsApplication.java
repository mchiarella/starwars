package com.b2w.starwars;

import com.b2w.starwars.service.RemoteService;
import com.fasterxml.jackson.annotation.JacksonInject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class StarwarsApplication {


	@Autowired
	RemoteService remoteService;


	@PostConstruct
	public void initApplication() {
		remoteService.initializeService();
	}

	public static void main(String[] args) {
		SpringApplication.run(StarwarsApplication.class, args);
	}
}
