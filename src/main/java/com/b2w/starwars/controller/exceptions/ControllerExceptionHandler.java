package com.b2w.starwars.controller.exceptions;

import com.b2w.starwars.service.exceptions.ExistingPlanetNameException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(ExistingPlanetNameException.class)
    public ResponseEntity<StandardError> databaseIntegrity ( ExistingPlanetNameException e, HttpServletRequest request){
        StandardError err = new StandardError(HttpStatus.BAD_REQUEST.value(), e.getMessage(),System.currentTimeMillis() );
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

}
