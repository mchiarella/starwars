package com.b2w.starwars.controller;

import com.b2w.starwars.domain.Planet;
import com.b2w.starwars.service.PlanetService;
import com.b2w.starwars.service.exceptions.ExistingPlanetNameException;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URISyntaxException;
import java.util.Optional;

/**
 * @author marcelo.chiarella@gmail.com
 * @updated 07/04/2018
 **/

@Controller
@RequestMapping("/planets")
public class PlanetController {

    private static PlanetService planetService;
    private final Logger LOG = LoggerFactory.getLogger(PlanetController.class);

    @Autowired
    public PlanetController(PlanetService planetService) {
        this.planetService = planetService;
    }


    /**
     * POST  Cria um  novo planeta.
     *
     * @param planet a ser criado
     * @return ResponseEntity com Status 201 (Created) e novo planeta no body, ou status 400 (Bad Request)
     * se o planeta já tiver um id ou se o nome estiver nulo, vazio ou somente espaços em branco.
     * @throws URISyntaxException se a sintaxe da URI estiver incorreta
     */

    @ApiOperation(value = "Cria um novo planeta")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody Planet planet, UriComponentsBuilder ucBuilder) throws ExistingPlanetNameException{

        LOG.info("creating new planet: {}", planet);
        Planet createdPlanet = planetService.create(planet);

        if (createdPlanet != null) {
            HttpHeaders headers = new HttpHeaders();
            headers
                    .setLocation(ucBuilder.path("/planets/{id}")
                            .buildAndExpand(createdPlanet.getId())
                            .toUri());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }


    /**
     * GET   Traz todos os planetas.
     *
     * @return ResponseEntity com status 200 (OK) e a lista de planetas no body.
     */

    @ApiOperation(value = "Retorna todos os planetas")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<Planet>> getAll(
            @RequestParam(value = "page", required = false, defaultValue = "0") String page,
            @RequestParam(value = "limit", required = false, defaultValue = "${page.size}") String limit,
            @RequestParam(value = "orderby", required = false, defaultValue = "name") String orderBy,
            @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction
    ) {

        LOG.info("getting all planets");
        PageRequest pageRequest = PageRequest.of(Integer.parseInt(page), Integer.parseInt(limit), Sort.Direction.valueOf(direction.toUpperCase()), orderBy);
        Page<Planet> planets = planetService.getAll(pageRequest);

        if (!planets.hasContent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(planets, HttpStatus.OK);
    }


    /**
     * GET Traz um ou mais planetas através da especificação de parte do seu nome.
     *
     * @return ResponseEntity com status 200 (OK) e o planeta no body, ou status 404 (Not Found).
     */

    @ApiOperation(value = "Retorna planetas por parte do nome")
    @RequestMapping(method = RequestMethod.GET, params = {"name"})
    public ResponseEntity<Page<Planet>> getByName(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "page", required = false, defaultValue = "0") String page,
            @RequestParam(value = "limit", required = false, defaultValue = "10") String limit,
            @RequestParam(value = "orderby", required = false, defaultValue = "name") String orderBy,
            @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction
    ) {
        PageRequest pageRequest = PageRequest.of(Integer.parseInt(page), Integer.parseInt(limit), Sort.Direction.valueOf(direction.toUpperCase()), orderBy);

        Page<Planet> planets = planetService.findByName(name, pageRequest);
        if (planets.hasContent()) {
            return new ResponseEntity<>(planets, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    /**
     * GET   Traz um planeta atraves do seu id.
     *
     * @return ResponseEntity com status 200 (OK) e o planeta no body, ou status 404 (Not Found).
     */

    @ApiOperation(value = "Retorna um planeta pelo seu id")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Planet> getById(@PathVariable String id) {

        LOG.info("getting planet");

        // findById evita o possível null do findOne.
        // Disponível no SpringBoot 2.x.x

        Optional<Planet> planet = planetService.findById(id);
        return planet.map(response ->
                ResponseEntity.ok().headers(null).body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * DELETE Deleta um planeta do repositório através do id.
     *
     * @return ResponseEntity com status 204 em caso de sucesso e 404 caso o resource não exista.
     */

    @ApiOperation(value = "Exclui um planeta pelo seu id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Planet> deletePlanet(@PathVariable String id) {

        if (planetService.findById(id).isPresent()) {
            planetService.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            // Preferi esse approach para um delete em um resource inexistente.
            // Há controvérsias sobre se deveríamos ou não continuar retornando 204 (noContent).
            return ResponseEntity.notFound().build();
        }
    }
}
